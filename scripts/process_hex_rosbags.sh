#!/bin/bash

rosrun bash_bot rosbag_info_txt

for file in *.bag; do 
	rosrun hexacopter rosbag_waypoint_times "$file" 5
	rosrun bash_bot rosbag_to_video $file mv_26805296
	rosrun bash_bot rosbag_to_video $file mv_26805460
done
