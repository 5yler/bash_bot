#!/bin/bash

# ROS image processing stuff
sudo apt-get install ros-indigo-image-view
sudo apt-get install ros-indigo-bag-tools

# video conversion stuff
sudo apt-get install mencoder
sudo -E add-apt-repository ppa:kirillshkrogalev/ffmpeg-next
sudo apt-get install ffmpeg
sudo apt-get install mjpegtools
sudo apt-get install ffmpeg2theora

# get more codecs
sudo apt-get install flac
sudo apt-get install libdvdcss2
sudo apt-get install yasm
sudo apt-get install libxvidcore-dev
sudo apt-get install libx264-dev
sudo apt-get install libbluray1
sudo apt-get install libbluray-dev
sudo apt-get install libfaac-dev
sudo apt-get install libjpeg-dev
sudo apt-get install ubuntu-restricted-extras
