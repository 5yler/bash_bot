#!/bin/bash

# source environment
source /opt/ros/indigo/setup.bash

# initialize workspace
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
catkin_init_workspace

cd ~/catkin_ws/
catkin_make

# source created setup file
source devel/setup.bash

echo "source ~/catkin_ws/devel/setup.bash # source catkin workspace" >> ~/.bashrc
