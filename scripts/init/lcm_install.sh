#! /bin/bash

cd ~

git clone https://github.com/lcm-proj/lcm.git
cd lcm
mkdir build && cd build
cmake ..
make -j4 -l4
sudo make install

echo "Done installing LCM"
