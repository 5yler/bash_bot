#!/bin/bash

sudo apt-add-repository universe
sudo apt-add-repository multiverse
sudo apt-add-repository restricted

sudo apt-get -y --force-yes update        # Fetches the list of available updates

sudo apt-get -y --force-yes install git nano samba exfat-fuse exfat-utils openssh-server tree gksu
sudo apt-get -y --force-yes install ncurses-bin libncurses5-dev

sudo ntpdate ntp.ubuntu.com # set ubuntu date server


# Setup Locale
sudo update-locale LANG=C LANGUAGE=C LC_ALL=C LC_MESSAGES=POSIX

# Setup sources.lst
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu trusty main" > /etc/apt/sources.list.d/ros-latest.list'

# Setup keys
wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add -

# Installation
sudo apt-get update
sudo apt-get -y --force-yes install ros-indigo-ros-base 
apt-cache search ros-indigo
sudo apt-get -y --force-yes install ros-indigo-navigation ros-indigo-robot-localization 
sudo apt-get -y --force-yes install ros-indigo-image-pipeline ros-indigo-image-view ros-indigo-laser-pipeline 
sudo apt-get -y --force-yes install ros-indigo-xacro ros-indigo-robot-state-publisher

sudo apt-get -y --force-yes install ros-indigo-rviz-* ros-indigo-rqt-*
sudo apt-get -y --force-yes install ros-indigo-xacro ros-indigo-robot-state-publisher

# Initialize rosdep
sudo apt-get -y --force-yes install python-rosdep 
sudo rosdep init

# To find available packages, use:
rosdep update

# Install rosinstall
sudo apt-get -y --force-yes install python-rosinstall 

sudo apt-get -y --force-yes update        # Fetches the list of available updates
sudo apt-get -y --force-yes upgrade       # Strictly upgrades the current packages


source /opt/ros/indigo/setup.bash

echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
source ~/.bashrc
cd ~

bash ~/bash_bot/scripts/init/create_catkin_workspace.sh

sudo ln -s ~/bash_bot ~/catkin_ws/src/
cd ~/catkin_ws/
catkin_make

echo "if [ -f ~/bash_bot/.robotrc ]; then
    . ~/bash_bot/.robotrc
fi
PS1=\$PSGreen # set prompt color " >> ~/.bashrc

# Add user to dialout and video groups
sudo adduser $USER dialout
sudo adduser $USER video

