#!/bin/bash

echo "if [ -f ~/bash_bot/.robotrc ]; then
	. ~/bash_bot/.robotrc
fi
PS1=\$PSRed # set prompt color " >> ~/.bashrc

echo "Added sourcing ~/bash_bot/.robotrc to ~/.bashrc"

read -p "Do you want to configure network proxy settings? " -n 1 -r
echo    # new line
if [[ $REPLY =~ ^[Yy]$ ]] 
then	
	cd ~/lab_network/
	bash proxy_settings_init.sh
fi

echo 
echo "Done."
