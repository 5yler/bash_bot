#!/bin/bash

sudo apt-get -y install ros-indigo-navigation ros-indigo-robot-localization  --force-yes
sudo apt-get -y install ros-indigo-image-pipeline ros-indigo-image-view ros-indigo-rqt-* --force-yes
sudo apt-get -y install ros-indigo-laser-pipeline ros-indigo-urg-node ros-indigo-hokuyo-node --force-yes
sudo apt-get -y install ros-indigo-husky-* --force-yes
