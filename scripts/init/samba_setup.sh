#!/bin/bash

# create Samba user
echo " [>  ] Create a user and password for Samba access."
sudo smbpasswd -a $USER

# back up config file
echo " [>> ] Backing up /etc/samba/smb.conf to file: /etc/samba/smb.conf.save"
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.save

echo " [>> ] Setting sharing settings for home directory..."
echo "[$(hostname)]
	path = /home/$USER/
	available = yes
	valid users = $USER
	read only = no
	browseable = yes
	public = yes
	writable = yes" | sudo tee -a /etc/samba/smb.conf > /dev/null

# restart Samba
echo " [>> ] Restarting Samba service..."
sudo service smbd restart

echo " [>>>] Done! Checking smb.conf for syntax errors..."
testparm



