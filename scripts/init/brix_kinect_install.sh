#!/bin/bash

read -p " [>  ] Do you want to clone kinect packages in an existing catkin workspace [Y], or create a new workspace [N]? " -n 1 -r
echo    # new line

if [[ $REPLY =~ ^[Yy]$ ]] 
then	
	read -p " [>  ] Are you in the desired workspace source directory (e.g. ~/catkin_ws/src)? [Y/N]? " -n 1 -r
	echo    # new line
	if [[ $REPLY =~ ^[Yy]$ ]] 
	then
		NEW_WS=`pwd`
	fi
	if [[ $REPLY =~ ^[Nn]$ ]] 
	then
		echo " [>  ] Please re-run the script from the desired source directory."
		exit 0
	fi
fi
if [[ $REPLY =~ ^[Nn]$ ]] 
then
	read -p " [>  ] Creating new catkin workspace in your user directory (~). Enter the name of the workspace to create: " -r
	echo    # new line
	NEW_WS=$HOME/$REPLY
	source /opt/ros/indigo/devel/setup.bash
	mkdir -p $NEW_WS/src && cd $NEW_WS/src
	catkin_init_workspace
	echo " [>  ] Initialized new workspace in "`pwd`

fi

echo " [>  ] Cloning iai_kinect2 repository"
git clone https://github.com/code-iai/iai_kinect2.git
echo " [>  ] Cloning IRAD RGB-D data collection repository"
git clone git@bitbucket.org:draper-autonomy-irad/rgbd_data.git
cd iai_kinect2/
echo " [>  ] Installing dependencies for iai_kinect2. (if you see warnings, this is expected, don't worry about it)"
rosdep install -r --from-paths .
echo " [>  ] Cloning libfreenect2 library into ~/libfreenect2"
cd ~
git clone https://github.com/OpenKinect/libfreenect2.git
cd libfreenect2/
echo " [>> ] Downloading dependencies for libfreenect2..."
cd depends; ./download_debs_trusty.sh
sudo apt-get install build-essential cmake pkg-config -y --force-yes
sudo dpkg -i debs/libusb*deb
sudo apt-get install libturbojpeg libjpeg-turbo8-dev -y --force-yes
sudo dpkg -i debs/libglfw3*deb; 
sudo apt-get install -f; 
sudo apt-get install libgl1-mesa-dri-lts-vivid -y --force-yes
sudo -E apt-add-repository ppa:floe/beignet; 
sudo apt-get update; 
sudo apt-get install beignet-dev -y --force-yes
sudo dpkg -i debs/ocl-icd*deb -y --force-yes
sudo apt-get install -f
sudo -E apt-add-repository ppa:floe/beignet; 
sudo apt-get update; 
# if you're wondering why some of these lines are duplicated, you're not alone
sudo apt-get install beignet-dev  -y --force-yes
sudo dpkg -i debs/ocl-icd*deb
sudo dpkg -i debs/{libva,i965}*deb; 
sudo apt-get install -f
sudo apt-get install libopenni2-dev -y --force-yes
echo " [>> ] (Hopefully?) done installing dependencies. If that didn't end up working, you might want to look at the source for this script and figure out where it broke."
echo " [>> ] Compiling libfreenect2..."
cd ..
mkdir build && cd build
cmake .. 
make
sudo make install
echo " [>> ] Done making and installing libfreenect2 into /usr/local!"
cd ~/libfreenect2/depends
sudo cp ../platform/linux/udev/90-kinect2.rules /etc/udev/rules.d/
echo " [>> ] Copied udev rules (90-kinect2.rules) into /etc/udev/rules.d/"
cd $NEW_WS
echo " [>> ] Running catkin_make in workspace..."
catkin_make
source devel/setup.bash
echo " [>> ] Done. Try running the kinect launch file to test: roslaunch kinect2_bridge kinect2_bridge.launch"
