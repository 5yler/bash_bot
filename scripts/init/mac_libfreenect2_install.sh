#!/bin/bash

# Download libfreenect2 source
cd ~/Downloads
git clone https://github.com/OpenKinect/libfreenect2.git
cd libfreenect2

# Install dependencies: libusb, GLFW
brew update
brew install libusb

# Install TurboJPEG (optional)
brew tap homebrew/versions
brew install glfw3
brew tap homebrew/science
brew install jpeg-turbo

# Install OpenNI2 (optional)
brew install openni2
echo "export OPENNI2_REDIST=/usr/local/lib/ni2
export OPENNI2_INCLUDE=/usr/local/include/ni2" >> ~/.bash_profile

# Build
mkdir build && cd build
cmake ..
make
make install
