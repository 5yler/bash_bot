#!/bin/bash

# ROS image processing stuff
sudo apt-get install -y ros-indigo-image-view
sudo apt-get install -y ros-indigo-bag-tools

# video conversion stuff
sudo apt-get install -y mencoder
sudo -E add-apt-repository ppa:kirillshkrogalev/ffmpeg-next
sudo apt-get install -y ffmpeg
sudo apt-get install -y mjpegtools
sudo apt-get install -y ffmpeg2theora

# get more codecs
sudo apt-get install -y flac libdvdcss2 yasm libxvidcore-dev libx264-dev libbluray1 libbluray-dev libfaac-dev libjpeg-dev
sudo apt-get install -y ubuntu-restricted-extras
