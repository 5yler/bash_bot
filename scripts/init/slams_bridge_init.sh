#!/bin/bash

cd ~
cp /media/$USER/E6/slams_bridge.tar.gz .
tar -xzf slams_bridge.tar.gz
cd slams_bridge/
chmod a+x rti_connext_dds-5.2.0-eval-x64Linux2.6gcc4.4.5.run
sudo ./rti_connext_dds-5.2.0-eval-x64Linux2.6gcc4.4.5.run
sudo cp squadx_rti_license.dat /opt/rti_connext_dds-5.2.0/
cp -r slams_ros_bridge/ ~/catkin_ws/src/

echo '#########################
# RTI Connext DDS Environment Setup

# Export RTI_LICENSE_FILE
export RTI_LICENSE_FILE="/opt/rti_connext_dds-5.2.0/squadx_rti_license.dat"

# Export NDDSHOME and PATH
export NDDSHOME="/opt/rti_connext_dds-5.2.0"
export PATH="$NDDSHOME/bin":$PATH

# Export library path
export LD_LIBRARY_PATH="$NDDSHOME/lib/x64Linux2.6gcc4.4.5":$LD_LIBRARY_PATH

#########################' >> ~/.bashrc

source ~/.bashrc

cd ~/catkin_ws

rm -r build;
rm -r devel;

catkin_make
